import 'package:flutter/cupertino.dart';

import 'core/interface/views/daily_calorie_threshold/daily_calorie_threshold_view.dart';
import 'core/interface/views/home/home_view.dart';
import 'core/interface/views/login/login_view.dart';
import 'core/interface/views/reports/reports_view.dart';
import 'core/interface/views/splash/splash_view.dart';

class Router {
  static final _routes = {
    SplashView.route: () => const SplashView(),
    HomeView.route: () => const HomeView(),
    LoginView.route: () => const LoginView(),
    DailyCalorieThresholdView.route: () => const DailyCalorieThresholdView(),
    ReportsView.route: () => const ReportsView(),
  };

  static Route generateRoute(RouteSettings settings) {
    return CupertinoPageRoute(
      builder: (context) {
        if (_routes[settings.name] != null) {
          return _routes[settings.name]!();
        }

        return Container();
      },
      settings: settings,
    );
  }
}
