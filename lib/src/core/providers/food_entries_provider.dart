import 'package:flutter/material.dart';
import 'package:simple_calorie_app/src/core/models/food_entry.dart';
import 'package:simple_calorie_app/src/core/providers/user_provider.dart';

import '../../locator.dart';
import '../models/user_data.dart';
import '../services/api_service.dart';
import '../services/hive_service.dart';

enum OrderBy { consumption, creation }

class FoodEntriesProvider extends ChangeNotifier {
  final apiService = locator<ApiService>();
  final userProvider = locator<UserProvider>();
  final hiveService = locator<HiveService>();

  List<FoodEntry> allFoodEntries =
      []; //list that will contain all documents from the logged in user
  late int count;
  OrderBy orderBy = OrderBy.creation;
  DateTimeRange? dateTimeRange;
  List<FoodEntry> filteredFoodEntries =
      []; //list that will contain the documents being shown in the frontend
  Map<DateTime, List<FoodEntry>> foodEntriesPerDay = {};

  //when user sign out
  void clearEntries() {
    filteredFoodEntries.clear();
    allFoodEntries.clear();
  }

  //interacting with firebase to retrieve all documents from the logged in user
  Future<ApiResponse> getUserFoodEntries() async {
    var response = await apiService.getUserFoodEntries(userProvider.user);
    if (response.status == Status.success) {
      allFoodEntries.clear();
      allFoodEntries.addAll(
        List<FoodEntry>.from(
          response.data.map(
            (foodEntry) => FoodEntry.fromFirebase(foodEntry),
          ),
        ),
      );
      count = allFoodEntries.length;
      var allFoodEntriesCopy = List<FoodEntry>.from(allFoodEntries);
      allFoodEntriesCopy.sort(
        (a, b) {
          if (a.dateTime.isBefore(b.dateTime)) {
            return -1;
          }
          return 1;
        },
      );
      for (var foodEntry in allFoodEntriesCopy) {
        var tempDate = DateTime(foodEntry.dateTime.year,
            foodEntry.dateTime.month, foodEntry.dateTime.day);
        if (foodEntriesPerDay.containsKey(tempDate)) {
          foodEntriesPerDay[tempDate]!.add(foodEntry);
        } else {
          foodEntriesPerDay[tempDate] = [foodEntry];
        }
      }

      return ApiResponse(
        status: Status.success,
      );
    }
    return ApiResponse(
      status: Status.error,
      errorCode: response.errorCode,
    );
  }

  //applying business logic (pagination and date filtering)
  void filterFoodEntries({
    required int offset,
  }) async {
    var allFoodEntriesCOPY = List<FoodEntry>.from(allFoodEntries);
    if (orderBy == OrderBy.consumption) {
      allFoodEntriesCOPY.sort((a, b) {
        if (a.dateTime.isBefore(b.dateTime)) {
          return -1;
        }
        return 1;
      });
    }
    filteredFoodEntries.clear();
    if (dateTimeRange == null) {
      for (var i = 0; i < allFoodEntriesCOPY.length; i++) {
        if (i < offset) {
          filteredFoodEntries.add(allFoodEntriesCOPY[i]);
        }
      }
    } else {
      filteredFoodEntries.addAll(
        allFoodEntriesCOPY.where(
          (element) =>
              element.dateTime.isBefore(dateTimeRange!.end) &&
              element.dateTime.isAfter(dateTimeRange!.start),
        ),
      );
    }
    await hiveService.removeFromBox(boxName: 'data', key: 'filtered_list');
    await hiveService.addToBox(
      key: 'filtered_list',
      value: filteredFoodEntries.map((foodEntry) => foodEntry.toMap()).toList(),
      boxName: 'data',
    );
    notifyListeners();
  }

  //get entries from hive
  Future<void> getEntriesFromCache() async {
    var temp =
        await hiveService.getDataFromBox(boxName: 'data', key: 'filtered_list');
    filteredFoodEntries = List<FoodEntry>.from(
      temp.map(
        (foodEntry) {
          return FoodEntry.fromCache(foodEntry);
        },
      ),
    );
    notifyListeners();
  }

  //switch the current order by
  void switchOrderBy(int offset) {
    if (orderBy == OrderBy.consumption) {
      orderBy = OrderBy.creation;
    } else {
      orderBy = OrderBy.consumption;
    }
    filterFoodEntries(offset: offset);
    notifyListeners();
  }

  //clear dates
  void clearDates(int offset) {
    dateTimeRange = null;
    filterFoodEntries(offset: offset);
    notifyListeners();
  }

  //set dateTimeRange
  void setDateTimeRange(DateTime start, DateTime end, int offset) {
    end = DateTime(end.year, end.month, end.day, 23, 59, 59);
    dateTimeRange = DateTimeRange(start: start, end: end);
    filterFoodEntries(offset: offset);
    notifyListeners();
  }

  //create food entry for logged in user
  Future<ApiResponse> createFoodEntry(
      String label, double calorieValue, DateTime date, int offset,
      [UserData? userToBeAddedTo]) async {
    var response = await apiService.createFoodEntry(
        userToBeAddedTo ?? userProvider.user, label, calorieValue, date);
    if (response.status == Status.success) {
      count += 1;
      FoodEntry foodEntry = FoodEntry(
          response.data,
          userToBeAddedTo?.uid ?? userProvider.user.uid,
          label,
          calorieValue,
          date,
          DateTime.now());

      //add on all food entries
      allFoodEntries.insert(0, foodEntry);

      //add on food entries per day
      var allFoodEntriesCopy = List<FoodEntry>.from(allFoodEntries);
      allFoodEntriesCopy.sort(
        (a, b) {
          if (a.dateTime.isBefore(b.dateTime)) {
            return -1;
          }
          return 1;
        },
      );
      foodEntriesPerDay.clear();
      for (var foodEntry in allFoodEntriesCopy) {
        var tempDate = DateTime(foodEntry.dateTime.year,
            foodEntry.dateTime.month, foodEntry.dateTime.day);
        if (foodEntriesPerDay.containsKey(tempDate)) {
          foodEntriesPerDay[tempDate]!.add(foodEntry);
        } else {
          foodEntriesPerDay[tempDate] = [foodEntry];
        }
      }
    }

    filterFoodEntries(offset: offset);
    notifyListeners();
    return response;
  }

  //update a specific food entry
  Future<ApiResponse> updateFoodEntry(FoodEntry foodEntry, String label,
      double calorieValue, DateTime date, int offset) async {
    var response = await apiService.updateFoodEntry(
      foodEntry.id,
      label,
      calorieValue,
      date,
    );
    if (response.status == Status.success) {
      var temp = FoodEntry(foodEntry.id, foodEntry.userId, label, calorieValue,
          date, DateTime.now());
      allFoodEntries[allFoodEntries
          .indexWhere((foodEntry) => foodEntry.id == foodEntry.id)] = temp;

      var allFoodEntriesCopy = List<FoodEntry>.from(allFoodEntries);
      allFoodEntriesCopy.sort(
        (a, b) {
          if (a.dateTime.isBefore(b.dateTime)) {
            return -1;
          }
          return 1;
        },
      );
      foodEntriesPerDay.clear();
      for (var foodEntry in allFoodEntriesCopy) {
        var tempDate = DateTime(foodEntry.dateTime.year,
            foodEntry.dateTime.month, foodEntry.dateTime.day);
        if (foodEntriesPerDay.containsKey(tempDate)) {
          foodEntriesPerDay[tempDate]!.add(foodEntry);
        } else {
          foodEntriesPerDay[tempDate] = [foodEntry];
        }
      }
    }
    filterFoodEntries(offset: offset);
    notifyListeners();
    return response;
  }

  //delete a specific food entry
  Future<ApiResponse> deleteFoodEntry(FoodEntry foodEntry, int offset) async {
    var response = await apiService.deleteFoodEntry(foodEntry.id);
    if (response.status == Status.success) {
      count -= 1;
      allFoodEntries.remove(foodEntry);
      var tempDate = DateTime(foodEntry.dateTime.year, foodEntry.dateTime.month,
          foodEntry.dateTime.day);
      foodEntriesPerDay[tempDate]!.remove(foodEntry);
      filterFoodEntries(offset: offset);
      notifyListeners();
    }
    return response;
  }
}
