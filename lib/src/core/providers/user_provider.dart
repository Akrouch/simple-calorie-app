import 'package:flutter/material.dart';

import '../../locator.dart';
import '../models/user_data.dart';
import '../services/api_service.dart';

class UserProvider extends ChangeNotifier {
  final apiService = locator<ApiService>();
  late UserData user;
  late String firebaseUserId;
  List<UserData> normalUsers = [];

  //AUTH SERVICE
  Future<ApiResponse> signIn(String email, String password) async {
    var response = await apiService.signIn(email, password);
    if (response.status == Status.success) {
      firebaseUserId = response.data;
    }
    return response;
  }

  Future<ApiResponse> getAllNormalUsers() async {
    var response = await apiService.getAllNormalUsers();
    if (response.status == Status.success) {
      normalUsers.clear();
      normalUsers.addAll(
        List<UserData>.from(
          response.data.map(
            (user) => UserData(user['uid'], user['type'],
                user['caloricThreshold'], user['email']),
          ),
        ),
      );
    }
    return response;
  }

  ApiResponse checkIfAuthenticated() {
    var response = apiService.checkIfAuthenticated();
    if (response.status == Status.success) {
      firebaseUserId = response.data;
    }
    return response;
  }

  Future<ApiResponse> signOut() async {
    return await apiService.signOut();
  }

  //getting user document from firestore using the unique firebaseAuth's user id
  Future<ApiResponse> getUserDocument() async {
    var response = await apiService.getUserDocument(firebaseUserId);
    if (response.status == Status.success) {
      user = UserData(firebaseUserId, response.data['type'],
          response.data['caloricThreshold'], response.data['email']);
    }
    return response;
  }
}
