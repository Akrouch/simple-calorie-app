class FoodEntry {
  late String id;
  late String userId;
  late String label;
  late double calorieValue;
  late DateTime dateTime;
  late DateTime createdAt;

  FoodEntry(this.id, this.userId, this.label, this.calorieValue, this.dateTime,
      this.createdAt);

  FoodEntry.fromFirebase(Map<String, dynamic> doc) {
    id = doc['id'];
    userId = doc['userId'];
    label = doc['label'];
    calorieValue = doc['calorieValue'].toDouble();
    dateTime = DateTime.parse(doc['dateTime'].toDate().toString());
    createdAt = DateTime.parse(doc['createdAt'].toDate().toString());
  }

  FoodEntry.fromCache(dynamic json) {
    id = json['id'];
    userId = json['userId'];
    label = json['label'];
    calorieValue = json['calorieValue'].toDouble();
    dateTime = DateTime.parse(json['dateTime']);
    createdAt = DateTime.parse(json['createdAt']);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'userId': userId,
      'label': label,
      'calorieValue': calorieValue,
      'dateTime': dateTime.toIso8601String(),
      'createdAt': createdAt.toIso8601String(),
    };
  }
}
