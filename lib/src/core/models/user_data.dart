enum Type { normal, admin }

class UserData {
  late String uid;
  Type? type;
  late String email;
  late double caloricThreshold;

  UserData(this.uid, String type, dynamic caloricThreshold, this.email) {
    this.type = type == 'NORMAL' ? Type.normal : Type.admin;
    this.caloricThreshold = caloricThreshold.toDouble();
  }

  UserData.fromCache(this.email);
}
