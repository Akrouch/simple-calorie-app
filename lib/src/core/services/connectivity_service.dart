import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

class ConnectivityService extends ChangeNotifier {
  late bool isConnected;
  final Connectivity _connectivity = Connectivity();

  Future<bool> checkConnectivity() async {
    var connectivityResult = await _connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      isConnected = true;
    } else {
      isConnected = false;
    }
    notifyListeners();
    return isConnected;
  }

  void constantlyCheckConnectivity() async {
    Timer.periodic(
      Duration(seconds: 10),
      (Timer t) async {
        await checkConnectivity();
        if (!isConnected) {
          t.cancel();
        }
      },
    );
  }
}
