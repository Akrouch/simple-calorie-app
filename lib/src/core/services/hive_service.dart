import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

class HiveService {
  Future initHive() async {
    var dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
    return Hive.openBox('data');
  }

  Future<bool> boxNotEmpty({required String boxName}) async {
    final openBox = await Hive.openBox(boxName);
    var length = openBox.length;
    return length != 0;
  }

  Future addToBox<T>({
    required String key,
    required var value,
    required String boxName,
  }) async {
    final openBox = await Hive.openBox(boxName);
    await openBox.put(key, value);
  }

  Future removeFromBox<T>(
      {required String boxName, required String key}) async {
    final openBox = await Hive.openBox(boxName);
    await openBox.delete(key);
  }

  Future clearBox({required String boxName}) async {
    await Hive.box(boxName).clear();
  }

  Future getDataFromBox<T>(
      {required String boxName, required String key}) async {
    final openBox = await Hive.openBox(boxName);
    return openBox.get(key);
  }

  Future getAllDataFromBox<T>({required String boxName}) async {
    var boxList = <T>[];
    final openBox = await Hive.openBox(boxName);
    var length = openBox.length;
    for (var i = 0; i < length; i++) {
      boxList.add(openBox.getAt(i));
    }
    return boxList;
  }
}
