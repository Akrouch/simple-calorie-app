import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../models/user_data.dart';

enum Status {
  error,
  success,
  timeout,
}

class ApiResponse {
  String? errorCode;
  final Status status;
  dynamic data;

  ApiResponse({
    this.errorCode,
    required this.status,
    this.data,
  });
}

class ApiService {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  //START AUTH METHODS
  Future<ApiResponse> signIn(String email, String password) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return ApiResponse(
        status: Status.success,
        data: _firebaseAuth.currentUser!.uid,
      );
    } on FirebaseAuthException catch (e) {
      print(e);
      return ApiResponse(
        status: Status.error,
        errorCode: e.code,
      );
    }
  }

  Future<ApiResponse> getAllNormalUsers() async {
    List<Map<String, dynamic>?> data = [];
    dynamic error;
    await _firebaseFirestore
        .collection('users')
        .where('type', isEqualTo: 'NORMAL')
        .get()
        .then((querySnapshot) {
      for (var result in querySnapshot.docs) {
        var temp = result.data();
        temp['uid'] = result.id;
        data.add(temp);
      }
    }).catchError((e) {
      error = e;
    });
    if (error == null) {
      return ApiResponse(
        status: Status.success,
        data: data,
      );
    }
    return ApiResponse(
      status: Status.error,
      errorCode: 'An error occurred. Check connection and try again.',
    );
  }

  ApiResponse checkIfAuthenticated() {
    var possibleUser = _firebaseAuth.currentUser;
    if (possibleUser == null) {
      return ApiResponse(
        status: Status.error,
      );
    }
    return ApiResponse(
      status: Status.success,
      data: possibleUser.uid,
    );
  }

  Future<ApiResponse> signOut() async {
    try {
      await _firebaseAuth.signOut();
      return ApiResponse(
        status: Status.success,
      );
    } on FirebaseAuthException catch (e) {
      return ApiResponse(
        status: Status.error,
        errorCode: e.code,
      );
    }
  }

  //START FIRESTORE METHODS
  //getting user document in firestore from firebase auth's unique user id
  Future<ApiResponse> getUserDocument(String uid) async {
    Map<String, dynamic>? data;
    dynamic error;
    await _firebaseFirestore.collection('users').doc(uid).get().then((value) {
      data = value.data();
    }).catchError((e) {
      error = e;
    });
    if (error == null) {
      return ApiResponse(
        status: Status.success,
        data: data,
      );
    }
    return ApiResponse(
      status: Status.error,
      errorCode: 'error',
    );
  }

  //retrieving all food entries from the looged in user OR all existing documents if user is ADMIN
  Future<ApiResponse> getUserFoodEntries(UserData user) async {
    List<Map<String, dynamic>?> data = [];
    dynamic error;
    if (user.type == Type.admin) {
      await _firebaseFirestore
          .collection('food-entries')
          .orderBy('createdAt', descending: true)
          .get()
          .then((querySnapshot) {
        for (var result in querySnapshot.docs) {
          var temp = result.data();
          temp['id'] = result.id;
          data.add(temp);
        }
      }).catchError((e) {
        error = e;
      });
    } else {
      await _firebaseFirestore
          .collection('food-entries')
          .orderBy('createdAt', descending: true)
          .where('userId', isEqualTo: user.uid)
          .get()
          .then((querySnapshot) {
        for (var result in querySnapshot.docs) {
          var temp = result.data();
          temp['id'] = result.id;
          data.add(temp);
        }
      }).catchError((e) {
        error = e;
      });
    }
    if (error == null) {
      return ApiResponse(
        status: Status.success,
        data: data,
      );
    }
    return ApiResponse(
      status: Status.error,
      data: error.toString(),
    );
  }

  //updating food entry
  Future<ApiResponse> updateFoodEntry(String foodEntryId, String label,
      double calorieValue, DateTime date) async {
    try {
      await _firebaseFirestore
          .collection('food-entries')
          .doc(foodEntryId)
          .update({
        'label': label,
        'calorieValue': calorieValue,
        'dateTime': date,
      });
      return ApiResponse(
        status: Status.success,
      );
    } catch (e) {
      return ApiResponse(
        status: Status.error,
        errorCode: 'Food entry could not be updated. Check your connection.',
      );
    }
  }

  //creating food entry
  Future<ApiResponse> createFoodEntry(
      UserData user, String label, double calorieValue, DateTime date) async {
    try {
      String? id;
      await _firebaseFirestore.collection('food-entries').add({
        'calorieValue': calorieValue,
        'dateTime': date,
        'label': label,
        'userId': user.uid,
        'createdAt': DateTime.now(),
      }).then((value) {
        id = value.id;
      });

      return ApiResponse(
        status: Status.success,
        data: id,
      );
    } catch (e) {
      return ApiResponse(
        status: Status.error,
        errorCode: 'Food entry could not be created. Check your connection.',
      );
    }
  }

  Future<ApiResponse> deleteFoodEntry(String foodEntryId) async {
    try {
      await _firebaseFirestore
          .collection('food-entries')
          .doc(foodEntryId)
          .delete();
      return ApiResponse(
        status: Status.success,
      );
    } catch (e) {
      return ApiResponse(
        status: Status.error,
        errorCode: 'Food entry could not be deleted. Check your connection.',
      );
    }
  }
}
