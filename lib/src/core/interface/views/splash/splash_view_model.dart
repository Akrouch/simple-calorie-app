import 'package:simple_calorie_app/src/core/interface/views/login/login_view.dart';
import 'package:simple_calorie_app/src/core/providers/food_entries_provider.dart';
import 'package:stacked/stacked.dart';

import '../../../../locator.dart';
import '../../../controllers/interface_controller.dart';
import '../../../models/user_data.dart';
import '../../../providers/user_provider.dart';
import '../../../services/api_service.dart';
import '../../../services/connectivity_service.dart';
import '../../../services/hive_service.dart';
import '../home/home_view.dart';

class SplashViewModel extends BaseViewModel {
  final userProvider = locator<UserProvider>();
  final interface = locator<InterfaceController>();
  final hiveService = locator<HiveService>();
  final foodEntriesProvider = locator<FoodEntriesProvider>();
  final connectivityService = locator<ConnectivityService>();

  void checkIfUserAuthenticated() async {
    await hiveService.initHive();
    await connectivityService.checkConnectivity();
    if (connectivityService.isConnected) {
      connectivityService.constantlyCheckConnectivity();
    }
    await Future.delayed(const Duration(seconds: 3));

    if (connectivityService.isConnected) {
      var response = userProvider
          .checkIfAuthenticated(); // checks if current JWT stored in cache is still valid (in order to proceed to home page)
      if (response.status == Status.success) {
        //if request was successful
        response = await userProvider
            .getUserDocument(); //attempts to get user document to populate user to check if it is NORMAL user or ADMIN
        if (response.status == Status.success) {
          //if request was successful
          await interface.replaceAllWith(HomeView
              .route); //navigates to home page to deal with retrieving food entries
          return;
        }
      }
      await interface.replaceAllWith(LoginView
          .route); //if user is not authenticated anymore OR if the app was unable to retrieve user's document, go to login
    } else {
      //checks if the email is in cache
      var emailInCache =
          await hiveService.getDataFromBox(boxName: 'data', key: 'email');
      print(emailInCache);
      //checks if previous list is in cache
      var filteredList = await hiveService.getDataFromBox(
          boxName: 'data', key: 'filtered_list');
      print(filteredList);
      if (emailInCache != null && filteredList != null) {
        userProvider.user = UserData.fromCache(emailInCache);
        await foodEntriesProvider.getEntriesFromCache();
        await interface.navigateTo(HomeView.route);
      } else {
        await interface.showDialogMessage(
          isDismissible: false,
          title: 'No internet connection!',
          message: 'Establish an internet connection and retry',
          actions: [
            DialogAction(
              onPressed: () {
                interface.goBack();
              },
              title: 'Retry',
            )
          ],
        );
        checkIfUserAuthenticated();
      }
    }
  }
}
