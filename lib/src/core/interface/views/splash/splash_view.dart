import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../shared/colors.dart';
import 'splash_view_model.dart';

class SplashView extends StatelessWidget {
  static const route = '/';

  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SplashViewModel>.reactive(
      onModelReady: (model) => model.checkIfUserAuthenticated(),
      viewModelBuilder: () => SplashViewModel(),
      builder: (context, model, child) => Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            color: AppColors.strongBlue,
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Hero(
                  tag: '1',
                  child: Image.asset(
                    'assets/logo.png',
                    width: MediaQuery.of(context).size.width * 0.7,
                    fit: BoxFit.fitWidth,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
