import 'package:stacked/stacked.dart';

import '../../../../locator.dart';
import '../../../controllers/interface_controller.dart';
import '../../../providers/user_provider.dart';
import '../../../services/api_service.dart';
import '../../../services/hive_service.dart';
import '../../shared/error_code_handler.dart';
import '../home/home_view.dart';

class LoginViewModel extends BaseViewModel {
  final interface = locator<InterfaceController>();
  final userProvider = locator<UserProvider>();
  final hiveService = locator<HiveService>();

  bool obscureText = true;
  String? password;
  String? email;

  void toggleObscureText() {
    obscureText = !obscureText;
    notifyListeners();
  }

  bool validateFields() {
    if (email == null) {
      interface.showTopSnackBar(
        message: 'Please, type in a valid e-mail',
        type: ToastType.error,
      );
      return false;
    }
    if (password == null) {
      interface.showTopSnackBar(
        message: 'Please, type in a valid password',
        type: ToastType.error,
      );
      return false;
    }
    return true;
  }

  void signIn() async {
    if (validateFields()) {
      //validates inputs
      interface.showLoader();
      var response = await userProvider.signIn(email!, password!);
      if (response.status == Status.success) {
        //checks if request was successful
        response = await userProvider
            .getUserDocument(); //then attempts to get user document to populate user and to see if it is ADMIN or NROMAL user
        if (response.status == Status.success) {
          //insert email into hive service for offline feature
          await hiveService.addToBox(
            key: 'email',
            value: userProvider.user.email,
            boxName: 'data',
          );

          interface.closeLoader();

          //checks if request was successful
          interface.navigateTo(HomeView.route); //if so, navigates to home page
          return;
        }
        interface.showTopSnackBar(
          message: errorCodeHandler(response.errorCode!),
          type: ToastType.error,
        );
        interface.closeLoader();

        return;
      }
      interface.showTopSnackBar(
        message: errorCodeHandler(response.errorCode!),
        type: ToastType.error,
      );
      interface.closeLoader();
    }
  }
}
