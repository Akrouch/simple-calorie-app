import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../shared/colors.dart';
import '../../widgets/custom_text_field.dart';
import 'login_view_model.dart';

class LoginView extends StatelessWidget {
  static const route = '/login';

  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<LoginViewModel>.reactive(
      viewModelBuilder: () => LoginViewModel(),
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 75),
            child: SizedBox(
              height: MediaQuery.of(context).size.height - 150,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 55),
                    child: Hero(
                      tag: '1',
                      child: Image.asset('assets/no-bg-logo-blue.png'),
                    ),
                  ),
                  SizedBox(height: 20),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      'Login to your Account',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  CustomTextField(
                    hintText: 'Email',
                    transparentBg: true,
                    onChanged: (v) {
                      if (v == '') {
                        model.email = null;
                      } else {
                        model.email = v;
                      }
                    },
                  ),
                  const SizedBox(height: 15),
                  CustomTextField(
                    hintText: 'Password',
                    transparentBg: true,
                    obscureText: model.obscureText,
                    onSuffixIconPressed: model.toggleObscureText,
                    suffixIcon: model.obscureText
                        ? Icons.visibility_outlined
                        : Icons.visibility_off_outlined,
                    onChanged: (v) {
                      if (v == '') {
                        model.password = null;
                      } else {
                        model.password = v;
                      }
                    },
                  ),
                  const SizedBox(height: 30),
                  SizedBox(
                    height: 48,
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      onPressed: () => model.signIn(),
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        primary: AppColors.strongBlue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6),
                        ),
                      ),
                      child: const Text(
                        'Log in',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
