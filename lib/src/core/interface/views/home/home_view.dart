import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simple_calorie_app/src/core/interface/views/home/widgets.dart';
import 'package:simple_calorie_app/src/core/providers/food_entries_provider.dart';
import 'package:simple_calorie_app/src/core/services/connectivity_service.dart';
import 'package:sliver_tools/sliver_tools.dart';
import 'package:stacked/stacked.dart';

import '../../../models/user_data.dart';
import '../../shared/colors.dart';
import 'home_view_model.dart';

class HomeView extends StatelessWidget {
  static const route = '/home';

  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      onModelReady: (model) => model.loadData(),
      viewModelBuilder: () => HomeViewModel(),
      builder: (context, model, child) => Consumer<ConnectivityService>(
        builder: (context, connectivityService, _) =>
            Consumer<FoodEntriesProvider>(
          builder: (context, foodEntriesProvider, _) {
            final _scrollController = ScrollController();
            _scrollController.addListener(
              () async {
                if (connectivityService.isConnected) {
                  if (_scrollController.position.extentAfter == 0 &&
                      foodEntriesProvider.filteredFoodEntries.length !=
                          foodEntriesProvider.count) {
                    model.offset += 10;

                    model.interface.showLoader();
                    await Future.delayed(Duration(milliseconds: 500));
                    foodEntriesProvider.filterFoodEntries(offset: model.offset);
                    model.interface.closeLoader();
                  }
                }
              },
            );
            return Scaffold(
              drawer: AppDrawer(),
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              body: CustomScrollView(
                controller: _scrollController,
                slivers: [
                  SliverAppBar(
                    iconTheme: IconThemeData(
                      color: AppColors.strongBlue,
                    ),
                    elevation: 0,
                    backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                  ),
                  SliverPadding(
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                    sliver: MultiSliver(
                      children: [
                        if (!connectivityService.isConnected)
                          RetryConnectionWidget()
                        else if (model.userProvider.user.type != null &&
                            model.userProvider.user.type == Type.normal)
                          Column(
                            children: [
                              TodaysProgress(),
                              SizedBox(height: 20),
                            ],
                          ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'FOOD ENTRIES',
                              style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),
                            ),
                            OrderByWidget(),
                          ],
                        ),
                        SizedBox(height: 10),
                        Container(
                          height: 1,
                          color: Colors.grey.shade300,
                        ),
                        SizedBox(height: 10),
                        DateRangeFilter(),
                        SizedBox(height: 15),
                        Column(
                          children: List.generate(
                            foodEntriesProvider.filteredFoodEntries.length,
                            (index) => SingleFoodEntry(index),
                          ),
                        ),
                        SizedBox(height: 50),
                      ],
                    ),
                  ),
                ],
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: FloatingButtons(),
            );
          },
        ),
      ),
    );
  }
}
