import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:simple_calorie_app/src/core/controllers/interface_controller.dart';
import 'package:simple_calorie_app/src/core/interface/views/daily_calorie_threshold/daily_calorie_threshold_view.dart';
import 'package:simple_calorie_app/src/core/interface/views/reports/reports_view.dart';
import 'package:simple_calorie_app/src/core/interface/widgets/custom_text_field.dart';
import 'package:simple_calorie_app/src/core/services/connectivity_service.dart';
import 'package:stacked/stacked.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../../locator.dart';
import '../../../models/food_entry.dart';
import '../../../models/user_data.dart';
import '../../../providers/food_entries_provider.dart';
import '../../../providers/user_provider.dart';
import '../../shared/colors.dart';
import '../../widgets/custom_drop_down.dart';
import '../../widgets/single_day_threshold_chart.dart';
import 'home_view_model.dart';

class AppDrawer extends ViewModelWidget<HomeViewModel> {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Drawer(
      backgroundColor: Colors.white,
      child: Container(
        padding: const EdgeInsets.fromLTRB(10, 40, 10, 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 15),
              child: Text(
                'Logged in as: ${locator<UserProvider>().user.email}',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                ),
              ),
            ),
            SizedBox(height: 20),
            Container(
              height: 1,
              color: Colors.grey.shade200,
            ),
            SizedBox(height: 10),
            if (locator<ConnectivityService>().isConnected)
              Column(
                children: [
                  DrawerItem(
                    asset: Icons.data_thresholding_outlined,
                    title: locator<UserProvider>().user.type == Type.admin
                        ? 'Reports'
                        : 'Progress of previous days',
                    onTap: () => locator<UserProvider>().user.type == Type.admin
                        ? viewModel.interface.navigateTo(ReportsView.route)
                        : viewModel.interface
                            .navigateTo(DailyCalorieThresholdView.route),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 1,
                    color: Colors.grey.shade200,
                  ),
                  SizedBox(height: 10),
                  DrawerItem(
                    asset: Icons.exit_to_app,
                    title: 'Logout',
                    onTap: () => viewModel.signOut(),
                  ),
                ],
              )
            else
              Container(
                padding: EdgeInsets.only(top: 50),
                width: double.infinity,
                child: Center(
                  child: Text(
                    'No connection mode',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

class DrawerItem extends StatelessWidget {
  final Function() onTap;
  final String title;
  final IconData asset;

  const DrawerItem({
    Key? key,
    required this.onTap,
    required this.title,
    required this.asset,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.translucent,
      child: Container(
        height: 40,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            Icon(
              asset,
              size: 20,
              color: AppColors.strongBlue,
            ),
            SizedBox(width: 15),
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Color.fromRGBO(52, 48, 62, 1),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SingleFoodEntry extends ViewModelWidget<HomeViewModel> {
  final int index;
  const SingleFoodEntry(this.index, [Key? key]) : super(key: key);

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Consumer<ConnectivityService>(
      builder: (context, connectivityService, _) =>
          Consumer<FoodEntriesProvider>(
        builder: (context, foodEntriesProvider, _) {
          var foodEntry = foodEntriesProvider.filteredFoodEntries[index];
          return Column(
            children: [
              GestureDetector(
                onTap: connectivityService.isConnected
                    ? () => viewModel.popNewFoodEntryModal(context, foodEntry)
                    : () => viewModel.noConnectionSnackBar(),
                child: Card(
                  margin: EdgeInsets.all(0),
                  elevation: 2,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              foodEntry.label,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: AppColors.strongBlue,
                              ),
                            ),
                            SizedBox(height: 7.5),
                            Text(
                              DateFormat('E, M/d/y - HH:mm')
                                  .format(foodEntry.dateTime),
                              style: TextStyle(
                                fontSize: 12,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              '${foodEntry.calorieValue.toStringAsFixed(2)} kcal',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                              ),
                            ),
                            SizedBox(height: 7.5),
                            Text(
                              'Creation: ${DateFormat('M/d/y').format(foodEntry.createdAt)}',
                              style: TextStyle(
                                fontSize: 12,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
            ],
          );
        },
      ),
    );
  }
}

class FloatingButtons extends ViewModelWidget<HomeViewModel> {
  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Consumer<ConnectivityService>(
      builder: (context, connectivityService, _) => Container(
        width: double.infinity,
        height: 55,
        margin: EdgeInsets.all(15),
        child: Align(
          alignment: Alignment.center,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: connectivityService.isConnected
                  ? AppColors.strongBlue
                  : Colors.grey,
              minimumSize: Size(double.infinity, 50),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            onPressed: viewModel.isBusy
                ? null
                : connectivityService.isConnected
                    ? () {
                        viewModel
                            .popUserSelectionForAdminFoodEntryModal(context);
                      }
                    : () => viewModel.noConnectionSnackBar(),
            child: Text(
              'New entry',
              style: TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class NewFoodEntryModal extends StatefulWidget {
  final Function(String, double, DateTime) onConfirmEntry;
  final Function()? deleteEntry;
  final FoodEntry? foodEntry;
  final String? ownerName;
  const NewFoodEntryModal({
    Key? key,
    required this.onConfirmEntry,
    this.foodEntry,
    this.deleteEntry,
    this.ownerName,
  }) : super(key: key);

  @override
  State<NewFoodEntryModal> createState() => _NewFoodEntryModalState();
}

class _NewFoodEntryModalState extends State<NewFoodEntryModal> {
  final TextEditingController _labelController = TextEditingController();
  final TextEditingController _calorieValueController = TextEditingController();
  DateTime? _date;

  @override
  void initState() {
    if (widget.foodEntry != null) {
      _labelController.text = widget.foodEntry!.label;
      _calorieValueController.text = widget.foodEntry!.calorieValue.toString();
      _date = widget.foodEntry!.dateTime;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            widget.foodEntry != null
                ? 'Edit food entry ${widget.ownerName != null ? 'owned by (${widget.ownerName})' : ''}'
                : 'New food entry ${widget.ownerName != null ? 'for (${widget.ownerName})' : ''}',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 15),
          CustomTextField(
            autoFocus: true,
            controller: _labelController,
            onChanged: (v) {},
            hintText: 'Label',
          ),
          SizedBox(height: 15),
          CustomTextField(
            controller: _calorieValueController,
            onChanged: (v) {},
            inputFormatters: [],
            keyboardType: TextInputType.number,
            hintText: 'Calorie value',
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () async {
              FocusScope.of(context).unfocus();
              _date ??= DateTime.now();
              await showModalBottomSheet(
                isScrollControlled: true,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(20),
                  ),
                ),
                context: context,
                builder: (context) => SizedBox(
                  height: 250,
                  child: CupertinoDatePicker(
                    use24hFormat: true,
                    mode: CupertinoDatePickerMode.dateAndTime,
                    onDateTimeChanged: (value) {
                      setState(() {
                        _date = value;
                      });
                    },
                    minuteInterval: 1,
                    minimumDate: DateTime.now().subtract(Duration(days: 365)),
                    initialDateTime: _date ?? DateTime.now(),
                    maximumDate: DateTime.now().add(Duration(days: 30)),
                  ),
                ),
              );
            },
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 0.5,
                    blurStyle: BlurStyle.normal,
                    spreadRadius: 3,
                    color: Colors.black.withOpacity(0.05),
                    offset: Offset(-1, 1),
                  )
                ],
              ),
              child: Text(
                _date == null
                    ? 'Select date of entry'
                    : DateFormat('E, M/d/y - HH:mm').format(_date!),
                style: TextStyle(
                  fontSize: 16,
                  color: _date == null
                      ? Color.fromRGBO(150, 152, 155, 1)
                      : Colors.black,
                ),
              ),
            ),
          ),
          SizedBox(height: 15),
          Container(
            width: double.infinity,
            height: 55,
            margin: EdgeInsets.only(
              bottom: widget.foodEntry == null
                  ? MediaQuery.of(context).viewInsets.bottom
                  : 0,
            ),
            child: Align(
              alignment: Alignment.center,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: AppColors.strongBlue,
                  minimumSize: Size(double.infinity, 50),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                onPressed: () {
                  if (_labelController.text == '') {
                    locator<InterfaceController>().showTopSnackBar(
                      message: 'Invalid label',
                      type: ToastType.warning,
                    );
                    return;
                  }
                  if (_calorieValueController.text == '' ||
                      _calorieValueController.text.contains('-') ||
                      _calorieValueController.text[0] == '.') {
                    locator<InterfaceController>().showTopSnackBar(
                      message: 'Invalid calorie value',
                      type: ToastType.warning,
                    );
                    return;
                  }

                  var calorieValue = 0.0;
                  try {
                    calorieValue = double.parse(double.parse(
                            _calorieValueController.text.replaceFirst(',', '.'))
                        .toStringAsFixed(2));
                  } catch (e) {
                    locator<InterfaceController>().showTopSnackBar(
                      message: 'Invalid calorie value',
                      type: ToastType.warning,
                    );
                    return;
                  }
                  if (_date == null) {
                    locator<InterfaceController>().showTopSnackBar(
                      message: 'Invalid date',
                      type: ToastType.warning,
                    );
                    return;
                  }
                  widget.onConfirmEntry(
                      _labelController.text, calorieValue, _date!);
                },
                child: Text(
                  widget.foodEntry != null ? 'Update entry' : 'New entry',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          if (widget.foodEntry != null)
            Container(
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom, top: 15),
              width: double.infinity,
              child: Align(
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: widget.deleteEntry,
                  child: Text(
                    'Delete',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.red,
                    ),
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}

class DatePickerPage extends StatefulWidget {
  final Function(DateRangePickerSelectionChangedArgs) onSelectedChanged;
  final PickerDateRange? initialSelectedRange;

  const DatePickerPage({
    Key? key,
    required this.onSelectedChanged,
    required this.initialSelectedRange,
  }) : super(key: key);

  @override
  State<DatePickerPage> createState() => _DatePickerPageState();
}

class _DatePickerPageState extends State<DatePickerPage> {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        SizedBox(
          height: 7.5,
          width: double.infinity,
        ),
        Center(
          child: Container(
            width: 100,
            height: 6,
            decoration: BoxDecoration(
              color: Color(0xffc8cacd),
              borderRadius: BorderRadius.circular(6),
            ),
          ),
        ),
        SizedBox(
          height: 11.5,
          width: double.infinity,
        ),
        SfDateRangePicker(
          maxDate: DateTime.now().add(Duration(days: 30)),
          minDate: DateTime.now().subtract(Duration(days: 365)),
          rangeSelectionColor: AppColors.strongBlue.withOpacity(0.3),
          endRangeSelectionColor: AppColors.strongBlue,
          yearCellStyle: DateRangePickerYearCellStyle(
            todayTextStyle: TextStyle(color: AppColors.strongBlue),
          ),
          monthViewSettings: DateRangePickerMonthViewSettings(
            viewHeaderStyle: DateRangePickerViewHeaderStyle(
                textStyle: TextStyle(fontSize: 10)),
          ),
          monthCellStyle: DateRangePickerMonthCellStyle(
            todayTextStyle: TextStyle(
              color: AppColors.strongBlue,
            ),
          ),
          startRangeSelectionColor: AppColors.strongBlue,
          selectionColor: AppColors.strongBlue,
          onSelectionChanged: widget.onSelectedChanged,
          selectionMode: DateRangePickerSelectionMode.range,
          todayHighlightColor: AppColors.strongBlue,
          initialSelectedRange: widget.initialSelectedRange,
        ),
        SizedBox(
          height: 11.5,
          width: double.infinity,
        ),
      ],
    );
  }
}

class DateRangeFilter extends ViewModelWidget<HomeViewModel> {
  const DateRangeFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Consumer<ConnectivityService>(
      builder: (context, connectivityService, _) =>
          Consumer<FoodEntriesProvider>(
        builder: (context, foodEntriesProvider, _) => Column(
          children: [
            SizedBox(
              width: double.infinity,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: connectivityService.isConnected
                    ? () async {
                        var response =
                            await viewModel.showDateRangePicker(context);
                        if (response is DateTimeRange) {
                          viewModel.interface.showLoader();
                          await Future.delayed(Duration(seconds: 1));
                          foodEntriesProvider.setDateTimeRange(
                              response.start, response.end, viewModel.offset);
                          viewModel.interface.closeLoader();
                        }
                      }
                    : () => viewModel.noConnectionSnackBar(),
                child: Row(
                  children: [
                    Text('From: '),
                    SizedBox(width: 5),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 0.5,
                              blurStyle: BlurStyle.normal,
                              spreadRadius: 3,
                              color: Colors.black.withOpacity(0.05),
                              offset: Offset(-1, 1),
                            )
                          ],
                        ),
                        child: (foodEntriesProvider.dateTimeRange != null)
                            ? Text(
                                DateFormat('MMM d, y').format(
                                    foodEntriesProvider.dateTimeRange!.start),
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              )
                            : Text(
                                'Select a date',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Color.fromRGBO(150, 152, 155, 1),
                                ),
                              ),
                      ),
                    ),
                    SizedBox(width: 10),
                    Text('To: '),
                    SizedBox(width: 5),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 0.5,
                              blurStyle: BlurStyle.normal,
                              spreadRadius: 3,
                              color: Colors.black.withOpacity(0.05),
                              offset: Offset(-1, 1),
                            )
                          ],
                        ),
                        child: (foodEntriesProvider.dateTimeRange != null)
                            ? Text(
                                DateFormat('MMM d, y').format(
                                    foodEntriesProvider.dateTimeRange!.end),
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              )
                            : Text(
                                'Select a date',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Color.fromRGBO(150, 152, 155, 1),
                                ),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            if (foodEntriesProvider.dateTimeRange != null)
              GestureDetector(
                onTap: () async {
                  viewModel.interface.showLoader();
                  await Future.delayed(Duration(seconds: 1));
                  foodEntriesProvider.clearDates(viewModel.offset);
                  viewModel.interface.closeLoader();
                },
                child: SizedBox(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        'Clear dates',
                        style: TextStyle(
                          color: AppColors.strongBlue,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

class OrderByWidget extends ViewModelWidget<HomeViewModel> {
  const OrderByWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Consumer<ConnectivityService>(
      builder: (context, connectivityService, _) =>
          Consumer<FoodEntriesProvider>(
        builder: (context, foodEntriesProvider, _) => SizedBox(
          child: Row(
            children: [
              Text(
                'Order by: ',
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                ),
              ),
              GestureDetector(
                onTap: connectivityService.isConnected
                    ? () async {
                        viewModel.interface.showLoader();
                        await Future.delayed(Duration(milliseconds: 300));
                        foodEntriesProvider.switchOrderBy(viewModel.offset);
                        viewModel.interface.closeLoader();
                      }
                    : () => viewModel.noConnectionSnackBar(),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 1),
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.yellowGreen),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Row(
                    children: [
                      Text(
                        foodEntriesProvider.orderBy == OrderBy.consumption
                            ? 'CONSUMPTION DATE'
                            : 'CREATION DATE',
                        style: TextStyle(
                          color: AppColors.yellowGreen,
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                        ),
                      ),
                      SizedBox(width: 5),
                      Icon(
                        foodEntriesProvider.orderBy == OrderBy.consumption
                            ? Icons.switch_right
                            : Icons.switch_left,
                        size: 22,
                        color: AppColors.yellowGreen,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class UserSelectionForAdminFoodEntryModal extends StatefulWidget {
  final List<UserData> normalUsers;
  final Function(UserData user) onUserSelected;
  final Function() onContinue;

  const UserSelectionForAdminFoodEntryModal({
    Key? key,
    required this.normalUsers,
    required this.onUserSelected,
    required this.onContinue,
  }) : super(key: key);

  @override
  State<UserSelectionForAdminFoodEntryModal> createState() =>
      _UserSelectionForAdminFoodEntryModalState();
}

class _UserSelectionForAdminFoodEntryModalState
    extends State<UserSelectionForAdminFoodEntryModal> {
  var selectedUserId;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Select the user to add new entry to:'),
          SizedBox(height: 5),
          CustomDropdownButton(
            maxHeight: 157.5,
            values: List.generate(
              widget.normalUsers.length,
              (index) => Option(
                id: widget.normalUsers[index].uid,
                title: widget.normalUsers[index].email,
              ),
            ),
            onSelect: (id) {
              setState(() {
                selectedUserId = id;
                widget.onUserSelected(widget.normalUsers
                    .firstWhere((element) => element.uid == id));
              });
            },
          ),
          SizedBox(height: 15),
          Container(
            margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            width: double.infinity,
            height: 55,
            child: Align(
              alignment: Alignment.center,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: AppColors.strongBlue,
                  minimumSize: Size(double.infinity, 50),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                onPressed:
                    selectedUserId == null ? null : () => widget.onContinue(),
                child: Text(
                  'Continue',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class TodaysProgress extends ViewModelWidget<HomeViewModel> {
  const TodaysProgress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Consumer<UserProvider>(
      builder: (context, userProvider, _) => Consumer<FoodEntriesProvider>(
        builder: (context, foodEntriesProvider, _) {
          var today = DateTime(
              DateTime.now().year, DateTime.now().month, DateTime.now().day);
          var todaysConsumption =
              foodEntriesProvider.foodEntriesPerDay.containsKey(today)
                  ? foodEntriesProvider.foodEntriesPerDay[today]!.fold(0.0,
                      (previousValue, element) {
                      var current = (previousValue as double);
                      return current += element.calorieValue.toDouble();
                    })
                  : 0;
          var limit = userProvider.user.caloricThreshold;

          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "TODAY'S PROGRESS",
                    style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                    ),
                  ),
                  GestureDetector(
                    onTap: () => viewModel.interface
                        .navigateTo(DailyCalorieThresholdView.route),
                    behavior: HitTestBehavior.translucent,
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5),
                          child: Text(
                            'Check out previous days',
                            style: TextStyle(
                              color: AppColors.yellowGreen,
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                            ),
                          ),
                        ),
                        SizedBox(width: 2),
                        Icon(
                          Icons.arrow_forward_ios_outlined,
                          size: 16,
                          color: AppColors.yellowGreen,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: 10),
              Container(
                height: 1,
                color: Colors.grey.shade300,
              ),
              SizedBox(height: 10),
              Card(
                margin: EdgeInsets.all(0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                elevation: 3,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'CALORIE CONSUMPTION',
                            style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(height: 2.5),
                          Text(
                            '${todaysConsumption.toStringAsFixed(2)}/${limit.toStringAsFixed(2)}',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                      SingleDayThresholdChart(
                        thatDayConsumption: todaysConsumption.toDouble(),
                        thatDayLimit: limit,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class RetryConnectionWidget extends ViewModelWidget<HomeViewModel> {
  const RetryConnectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Column(
      children: [
        Container(
          height: 65,
          width: double.infinity,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.red,
            borderRadius: BorderRadius.circular(6),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'No connection mode',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    'Reconnected to the internet?',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
              GestureDetector(
                onTap: () => viewModel.onRestartApp(),
                child: Container(
                  padding: EdgeInsets.all(7.5),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Center(
                    child: Text(
                      'RESTART APP',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
      ],
    );
  }
}
