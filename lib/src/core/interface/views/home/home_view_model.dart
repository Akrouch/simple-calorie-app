import 'package:flutter/material.dart';
import 'package:simple_calorie_app/src/core/interface/shared/error_code_handler.dart';
import 'package:simple_calorie_app/src/core/interface/views/home/widgets.dart';
import 'package:simple_calorie_app/src/core/interface/views/login/login_view.dart';
import 'package:stacked/stacked.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../../locator.dart';
import '../../../controllers/interface_controller.dart';
import '../../../models/food_entry.dart';
import '../../../models/user_data.dart';
import '../../../providers/food_entries_provider.dart';
import '../../../providers/user_provider.dart';
import '../../../services/api_service.dart';
import '../../../services/connectivity_service.dart';
import '../../../services/hive_service.dart';
import '../splash/splash_view.dart';

class HomeViewModel extends BaseViewModel {
  final userProvider = locator<UserProvider>();
  final foodEntriesProvider = locator<FoodEntriesProvider>();
  final hiveService = locator<HiveService>();
  final interface = locator<InterfaceController>();
  final connectivityService = locator<ConnectivityService>();

  int offset = 10;

  void loadData() async {
    if (connectivityService.isConnected) {
      setBusy(true);
      interface.showLoader();
      //if logged user is ADMIN, retrieves all users
      if (userProvider.user.type == Type.admin) {
        var response = await userProvider.getAllNormalUsers();
      }
      // retrieving all food entries from the logged in user OR all existing food entries if user is ADMIN
      var response = await foodEntriesProvider.getUserFoodEntries();
      if (response.status == Status.success) {
        //applying initial business logic to front end -> getting only the 10 first food entries (pagination simulation) from api
        foodEntriesProvider.filterFoodEntries(offset: offset);

        setBusy(false);
        interface.closeLoader();
        return;
      }
      setBusy(false);
      interface.closeLoader();
      await interface.showDialogMessage(
        title: 'An error occurred!',
        message: 'Check your connection and retry',
        actions: [
          DialogAction(
            onPressed: () {
              interface.goBack();
            },
            title: 'Retry',
          )
        ],
      );
      loadData();
    }
  }

  void signOut() async {
    userProvider.signOut();
    //clearing all cache
    await hiveService.clearBox(boxName: 'data');
    foodEntriesProvider.dateTimeRange = null;
    foodEntriesProvider.orderBy = OrderBy.creation;
    foodEntriesProvider.foodEntriesPerDay = {};
    foodEntriesProvider.clearEntries();
    interface.replaceAllWith(LoginView.route);
  }

  void popUserSelectionForAdminFoodEntryModal(BuildContext context,
      [FoodEntry? foodEntry]) async {
    if (userProvider.user.type == Type.normal) {
      popNewFoodEntryModal(context, foodEntry);
      return;
    }
    UserData? user;
    await showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (context) => UserSelectionForAdminFoodEntryModal(
        normalUsers: userProvider.normalUsers,
        onUserSelected: (u) => user = u,
        onContinue: () {
          interface.goBack();
          popNewFoodEntryModal(context, null, user);
        },
      ),
    );
  }

  void popNewFoodEntryModal(BuildContext context,
      [FoodEntry? foodEntry, UserData? userToBeAddedTo]) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (context) => NewFoodEntryModal(
        foodEntry: foodEntry,
        deleteEntry: () {
          deleteFoodEntry(foodEntry!);
        },
        ownerName: userToBeAddedTo != null
            ? userToBeAddedTo.email
            : getOwnerName(foodEntry),
        onConfirmEntry: (label, calorieValue, date) async {
          if (foodEntry == null) {
            createFoodEntry(
              label,
              calorieValue,
              date,
              userToBeAddedTo,
            );
          } else {
            updateFoodEntry(
              foodEntry,
              label,
              calorieValue,
              date,
            );
          }
        },
      ),
    );
  }

  String? getOwnerName(FoodEntry? foodEntry) {
    if (foodEntry == null) {
      return null;
    }
    if (userProvider.user.type != Type.admin) {
      return null;
    }
    return userProvider.normalUsers
        .firstWhere((normalUser) => normalUser.uid == foodEntry.userId)
        .email;
  }

  void updateFoodEntry(FoodEntry foodEntry, String label, double calorieValue,
      DateTime date) async {
    interface.showLoader();
    var response = await foodEntriesProvider.updateFoodEntry(
        foodEntry, label, calorieValue, date, offset);
    interface.closeLoader();
    if (response.status == Status.success) {
      interface.goBack();
      interface.showTopSnackBar(
        message: 'Successfully updated entry',
        type: ToastType.success,
      );
      return;
    }
    interface.showTopSnackBar(
      message: errorCodeHandler(response.errorCode!),
      type: ToastType.error,
    );
  }

  void deleteFoodEntry(FoodEntry foodEntry) async {
    interface.showLoader();
    var response = await foodEntriesProvider.deleteFoodEntry(foodEntry, offset);
    interface.closeLoader();
    if (response.status == Status.success) {
      interface.goBack();
      interface.showTopSnackBar(
        message: 'Successfully deleted entry',
        type: ToastType.success,
      );
      return;
    }
    interface.showTopSnackBar(
      message: errorCodeHandler(response.errorCode!),
      type: ToastType.error,
    );
  }

  void createFoodEntry(String label, double calorieValue, DateTime date,
      [UserData? userToBeAddedTo]) async {
    interface.showLoader();
    var response = await foodEntriesProvider.createFoodEntry(
      label,
      calorieValue,
      date,
      offset,
      userToBeAddedTo,
    );
    interface.closeLoader();
    if (response.status == Status.success) {
      interface.goBack();
      interface.showTopSnackBar(
        message: 'Successfully created new entry',
        type: ToastType.success,
      );
      return;
    }
    interface.showTopSnackBar(
      message: errorCodeHandler(response.errorCode!),
      type: ToastType.error,
    );
  }

  Future<DateTimeRange?> showDateRangePicker(
    context,
  ) async {
    DateTimeRange? dateRange;
    await showModalBottomSheet(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      context: context,
      builder: (context) => DatePickerPage(
        onSelectedChanged: (args) {
          if (args.value is PickerDateRange) {
            PickerDateRange pickerDateRange = args.value;
            if (pickerDateRange.startDate != null) {
              dateRange = DateTimeRange(
                  start: pickerDateRange.startDate!,
                  end: pickerDateRange.endDate ?? pickerDateRange.startDate!);
            }
          }
        },
        initialSelectedRange: foodEntriesProvider.dateTimeRange == null
            ? null
            : PickerDateRange(
                foodEntriesProvider.dateTimeRange!.start,
                foodEntriesProvider.dateTimeRange!.end,
              ),
      ),
    );
    return dateRange;
  }

  void onRestartApp() async {
    if (connectivityService.isConnected) {
      await hiveService.clearBox(boxName: 'data');
    }
    foodEntriesProvider.dateTimeRange = null;
    foodEntriesProvider.orderBy = OrderBy.creation;
    foodEntriesProvider.foodEntriesPerDay = {};
    foodEntriesProvider.clearEntries();
    interface.replaceAllWith(SplashView.route);
  }

  void noConnectionSnackBar() {
    interface.showBottomSnackBar(
      message: 'You are not connected to the internet!',
      backgroundColor: Colors.red,
    );
  }
}
