import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simple_calorie_app/src/core/interface/views/daily_calorie_threshold/widgets.dart';
import 'package:sliver_tools/sliver_tools.dart';

import '../../../providers/food_entries_provider.dart';
import '../../shared/colors.dart';

class DailyCalorieThresholdView extends StatelessWidget {
  static const route = '/dailyCalorieThreshold';

  const DailyCalorieThresholdView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<FoodEntriesProvider>(
      builder: (context, foodEntriesProvider, _) => Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              iconTheme: IconThemeData(color: AppColors.strongBlue),
              elevation: 0,
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              title: Text(
                'Previous days',
                style: TextStyle(color: AppColors.strongBlue),
              ),
              centerTitle: true,
            ),
            SliverPadding(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
              sliver: MultiSliver(
                children: List.generate(
                  foodEntriesProvider.foodEntriesPerDay.entries
                      .toList()
                      .reversed
                      .length,
                  (index) => Column(
                    children: [
                      SingleDayProgress(index),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
