import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:simple_calorie_app/src/core/providers/food_entries_provider.dart';

import '../../../providers/user_provider.dart';
import '../../widgets/single_day_threshold_chart.dart';

class SingleDayProgress extends StatelessWidget {
  final int index;
  const SingleDayProgress(this.index, [Key? key]) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(
      builder: (context, userProvider, _) => Consumer<FoodEntriesProvider>(
        builder: (context, foodEntriesProvider, _) {
          var entry = foodEntriesProvider.foodEntriesPerDay.entries
              .toList()
              .reversed
              .toList()[index];
          var limit = userProvider.user.caloricThreshold;
          var consumption = foodEntriesProvider.foodEntriesPerDay[entry.key]!
              .fold(0.0, (previousValue, element) {
            var current = (previousValue as double);
            return current += element.calorieValue.toDouble();
          });
          return Card(
            margin: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            elevation: 3,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        DateFormat('MMM d, y').format(entry.key),
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 2.5),
                      Text(
                        '${consumption.toStringAsFixed(2)}/${limit.toStringAsFixed(2)}',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                  SingleDayThresholdChart(
                    thatDayConsumption: consumption.toDouble(),
                    thatDayLimit: limit,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
