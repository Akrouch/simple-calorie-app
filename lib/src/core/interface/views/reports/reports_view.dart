import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simple_calorie_app/src/core/interface/views/reports/reports_view_model.dart';
import 'package:simple_calorie_app/src/core/interface/views/reports/widgets.dart';
import 'package:sliver_tools/sliver_tools.dart';
import 'package:stacked/stacked.dart';

import '../../../providers/food_entries_provider.dart';
import '../../shared/colors.dart';

class ReportsView extends StatelessWidget {
  static const route = '/reportsView';

  const ReportsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ReportsViewModel>.reactive(
      viewModelBuilder: () => ReportsViewModel(),
      onModelReady: (model) => model.loadData(),
      builder: (context, model, child) => Scaffold(
        body: Consumer<FoodEntriesProvider>(
          builder: (context, foodEntriesProvider, _) => Scaffold(
            body: model.isBusy
                ? Container()
                : CustomScrollView(
                    slivers: [
                      SliverAppBar(
                        iconTheme: IconThemeData(color: AppColors.strongBlue),
                        elevation: 0,
                        backgroundColor:
                            Theme.of(context).scaffoldBackgroundColor,
                        title: Text(
                          'Reports',
                          style: TextStyle(color: AppColors.strongBlue),
                        ),
                        centerTitle: true,
                      ),
                      SliverPadding(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
                        sliver: MultiSliver(
                          children: [
                            FoodEntriesAnalysis(),
                            SizedBox(height: 30),
                            CaloriesAnalysis(),
                          ],
                        ),
                      )
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
