import 'package:flutter/material.dart';
import 'package:simple_calorie_app/src/core/interface/views/reports/reports_view_model.dart';
import 'package:stacked/stacked.dart';

import '../../shared/colors.dart';

class FoodEntriesAnalysis extends ViewModelWidget<ReportsViewModel> {
  const FoodEntriesAnalysis({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ReportsViewModel viewModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'FOOD ENTRIES ANALYSIS',
          style: TextStyle(
            color: Colors.black54,
            fontWeight: FontWeight.w500,
            fontSize: 14,
          ),
        ),
        SizedBox(height: 5),
        Container(
          height: 1,
          color: Colors.grey.shade300,
        ),
        SizedBox(height: 15),
        Text(
          'Last 7 days (today included)',
          style: TextStyle(
            color: Colors.black54,
            fontWeight: FontWeight.w500,
            fontSize: 14,
          ),
        ),
        SizedBox(height: 10),
        Container(
          height: 35,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.grey.shade200,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(17.5),
              bottomRight: Radius.circular(17.5),
            ),
          ),
          alignment: Alignment.centerLeft,
          child: Stack(
            children: [
              FractionallySizedBox(
                widthFactor:
                    viewModel.last7daysEntries > viewModel.weekBeforeThatEntries
                        ? 1
                        : viewModel.weekBeforeThatEntries > 0
                            ? viewModel.last7daysEntries /
                                viewModel.weekBeforeThatEntries
                            : 0,
                heightFactor: 1,
                alignment: Alignment.centerLeft,
                child: Container(
                  decoration: BoxDecoration(
                    color: AppColors.yellowGreen,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(17.5),
                      bottomRight: Radius.circular(17.5),
                    ),
                  ),
                  alignment: Alignment.centerLeft,
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                alignment: Alignment.centerLeft,
                child: Text(
                  '${viewModel.last7daysEntries} entries',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(height: 20),
        Text(
          'One week before that',
          style: TextStyle(
            color: Colors.black54,
            fontWeight: FontWeight.w500,
            fontSize: 14,
          ),
        ),
        SizedBox(height: 10),
        Container(
          height: 35,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.grey.shade200,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(17.5),
              bottomRight: Radius.circular(17.5),
            ),
          ),
          alignment: Alignment.centerLeft,
          child: Stack(
            children: [
              FractionallySizedBox(
                widthFactor:
                    viewModel.weekBeforeThatEntries > viewModel.last7daysEntries
                        ? 1
                        : viewModel.last7daysEntries > 0
                            ? viewModel.weekBeforeThatEntries /
                                viewModel.last7daysEntries
                            : 0,
                heightFactor: 1,
                alignment: Alignment.centerLeft,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: AppColors.strongBlue,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(17.5),
                          bottomRight: Radius.circular(17.5),
                        ),
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                width: 100,
                alignment: Alignment.centerLeft,
                child: Text(
                  '${viewModel.weekBeforeThatEntries} entries',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class CaloriesAnalysis extends ViewModelWidget<ReportsViewModel> {
  const CaloriesAnalysis({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ReportsViewModel viewModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'CALORIES ANALYSIS',
          style: TextStyle(
            color: Colors.black54,
            fontWeight: FontWeight.w500,
            fontSize: 14,
          ),
        ),
        SizedBox(height: 5),
        Container(
          height: 1,
          color: Colors.grey.shade300,
        ),
        SizedBox(height: 15),
        Text(
          'Average number of calories per user (last 7 days)',
          style: TextStyle(
            color: Colors.black54,
            fontWeight: FontWeight.w500,
            fontSize: 14,
          ),
        ),
        SizedBox(height: 10),
        Text(
            '${viewModel.averageNumberOfCaloriesPerUser.toStringAsFixed(2)} kcal per user')
      ],
    );
  }
}
