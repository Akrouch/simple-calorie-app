import 'package:simple_calorie_app/src/core/providers/food_entries_provider.dart';
import 'package:stacked/stacked.dart';

import '../../../../locator.dart';
import '../../../controllers/interface_controller.dart';
import '../../../providers/user_provider.dart';

class ReportsViewModel extends BaseViewModel {
  final foodEntriesProvider = locator<FoodEntriesProvider>();
  final userProvider = locator<UserProvider>();
  final interface = locator<InterfaceController>();

  late int last7daysEntries;
  late int weekBeforeThatEntries;
  late double averageNumberOfCaloriesPerUser;

  void loadData() {
    setBusy(true);
    interface.showLoader();

    //BOTTOM LIMIT FOR 7 DAYS
    var last7daysBottomLimit =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
            .subtract(Duration(days: 7));

    //TOP LIMIT FOR 7 DAYS
    var last7DaysUpperLimit = DateTime(DateTime.now().year,
        DateTime.now().month, DateTime.now().day, 23, 59, 59);

    //BOTTOM LIMIT FOR WEEK BEFORE
    var weekBeforeThatEntriesBottomLimit =
        last7daysBottomLimit.subtract(Duration(days: 7));

    //GRAB ALL LAST 7 DAYS ENTRIES
    last7daysEntries = foodEntriesProvider.foodEntriesPerDay.entries
        .where(
          (fEntry) =>
              (fEntry.key.isAtSameMomentAs(last7daysBottomLimit) ||
                  fEntry.key.isAfter(last7daysBottomLimit)) &&
              (fEntry.key.isBefore(last7DaysUpperLimit)),
        )
        .fold(0,
            (previousValue, element) => previousValue += element.value.length);

    //GRAB ALL WEEK BEFORE ENTRIES
    weekBeforeThatEntries = foodEntriesProvider.foodEntriesPerDay.entries
        .where(
          (fEntry) =>
              fEntry.key.isBefore(last7daysBottomLimit) &&
              (fEntry.key.isAtSameMomentAs(weekBeforeThatEntriesBottomLimit) ||
                  fEntry.key.isAfter(weekBeforeThatEntriesBottomLimit)),
        )
        .fold(0,
            (previousValue, element) => previousValue += element.value.length);

    //AVERAGE NUMBER OF CALORIES PER USER IN THE LAST 7 DAYS
    averageNumberOfCaloriesPerUser =
        foodEntriesProvider.foodEntriesPerDay.entries
            .where(
      (fEntry) =>
          (fEntry.key.isAtSameMomentAs(last7daysBottomLimit) ||
              fEntry.key.isAfter(last7daysBottomLimit)) &&
          (fEntry.key.isBefore(last7DaysUpperLimit) ||
              fEntry.key.isAtSameMomentAs(last7DaysUpperLimit)),
    )
            .fold(0.0, (previousValue, element) {
      return previousValue +=
          element.value.fold(0.0, (previousValue2, element) {
        return previousValue2 += element.calorieValue;
      });
    });
    averageNumberOfCaloriesPerUser = double.parse(
        (averageNumberOfCaloriesPerUser / userProvider.normalUsers.length)
            .toStringAsFixed(2));
    interface.closeLoader();
    setBusy(false);
  }
}
