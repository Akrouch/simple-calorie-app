import 'package:flutter/material.dart';

import '../shared/colors.dart';

class Option {
  final String title;
  final dynamic id;

  Option({required this.title, required this.id});
}

class CustomDropdownButton extends StatefulWidget {
  final List<Option> values;
  final void Function(dynamic value) onSelect;
  final String? initialValue;
  final double? maxHeight;
  final bool? hasShadow;

  const CustomDropdownButton({
    required this.values,
    required this.onSelect,
    this.initialValue,
    this.maxHeight,
    this.hasShadow,
  });

  @override
  _CustomDropdownButtonState createState() => _CustomDropdownButtonState();
}

class _CustomDropdownButtonState extends State<CustomDropdownButton> {
  bool expanded = false;
  String? selected;
  String? errorText;
  late bool hasShadow;

  @override
  void initState() {
    super.initState();
    if (widget.initialValue != null) {
      selected = widget.initialValue;
    }
    hasShadow = widget.hasShadow ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        setState(() {
          expanded = !expanded;
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AnimatedContainer(
            height: 50,
            duration: Duration(milliseconds: 150),
            curve: Curves.easeOut,
            alignment: Alignment.center,
            width: double.maxFinite,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                if (hasShadow)
                  BoxShadow(
                    blurRadius: 10,
                    spreadRadius: 1,
                    color: Colors.black26,
                    offset: Offset(1, 1),
                  )
              ],
              borderRadius: BorderRadius.circular(18),
              border: Border.all(
                width: 1,
                color: Color.fromRGBO(232, 232, 234, 1),
              ),
            ),
            child: TextField(
              enabled: false,
              textAlignVertical: TextAlignVertical.center,
              style: TextStyle(
                fontSize: 12,
              ),
              decoration: InputDecoration(
                hintText: selected ?? 'Select user',
                contentPadding: EdgeInsets.all(15),
                border: InputBorder.none,
                hintStyle: TextStyle(
                  fontSize: 12,
                  color: selected != null
                      ? AppColors.black
                      : Color.fromRGBO(150, 152, 155, 1),
                ),
                suffixIcon: Icon(
                  !expanded
                      ? Icons.keyboard_arrow_down
                      : Icons.keyboard_arrow_up,
                  size: 20,
                  color: Color(0xff615e69),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: AnimatedContainer(
              height: expanded ? widget.values.length * 45.0 : 0,
              duration: Duration(milliseconds: 150),
              constraints: BoxConstraints(
                  maxHeight: widget.maxHeight ?? double.infinity),
              curve: Curves.easeOut,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: ListView(
                padding: EdgeInsets.zero,
                children: List<Widget>.from(
                  widget.values.map(
                    (value) => GestureDetector(
                      onTap: () {
                        setState(() {
                          selected = value.title;
                          expanded = false;
                          widget.onSelect(value.id);
                        });
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Container(
                        height: 45,
                        padding: EdgeInsets.only(left: 10),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          value.title,
                          style:
                              TextStyle(color: AppColors.black, fontSize: 12),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
