import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

import '../shared/colors.dart';

class SingleDayThresholdChart extends StatelessWidget {
  final double thatDayConsumption;
  final double thatDayLimit;
  const SingleDayThresholdChart(
      {Key? key, required this.thatDayConsumption, required this.thatDayLimit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 75,
      width: 75,
      child: PieChart(
        dataMap: {
          'consumed': thatDayConsumption / thatDayLimit,
          'limit': 1 - thatDayConsumption / thatDayLimit,
        },
        chartType: ChartType.ring,
        chartLegendSpacing: 1050,
        animationDuration: Duration(seconds: 3),
        initialAngleInDegree: 0,
        ringStrokeWidth: 7.5,
        centerText:
            '${((thatDayConsumption / thatDayLimit) * 100).toStringAsFixed(0)}%',
        centerTextStyle: TextStyle(
          fontSize: 10,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
        legendOptions: LegendOptions(showLegends: false),
        colorList: [
          1 - thatDayConsumption / thatDayLimit < 0.10
              ? Colors.red
              : 1 - thatDayConsumption / thatDayLimit < 0.25
                  ? Colors.amber
                  : 1 - thatDayConsumption / thatDayLimit < 0.5
                      ? Colors.yellow
                      : AppColors.strongBlue,
          AppColors.strongBlue.withOpacity(0.3),
        ],
        chartValuesOptions: ChartValuesOptions(showChartValues: false),
      ),
    );
  }
}
