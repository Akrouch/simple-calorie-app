import 'package:flutter/material.dart';

import '../shared/colors.dart';

class OnboardingBackground extends StatelessWidget {
  final double height;

  const OnboardingBackground({Key? key, required this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var ratio = MediaQuery.of(context).size.aspectRatio;
    return Container(
      margin: EdgeInsets.only(top: ratio),
      height: height,
      width: double.infinity,
      child: Image.asset(
        'assets/onboarding_background.png',
        width: double.infinity,
        fit: BoxFit.fitWidth,
        color: AppColors.strongBlue,
      ),
    );
  }
}
