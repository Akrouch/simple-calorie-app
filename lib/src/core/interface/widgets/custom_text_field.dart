import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../shared/colors.dart';

class CustomTextField extends StatefulWidget {
  final String? hintText;
  final Function(String) onChanged;
  final String? initialValue;
  final bool? enabled;
  final Widget? prefix;
  final Function()? onEditingComplete;
  final String? errorText;
  final Function()? onSuffixIconPressed;
  final TextInputType? keyboardType;
  final bool? transparentBg;
  final TextEditingController? controller;
  final bool? autoFocus;

  final List<TextInputFormatter>? inputFormatters;
  final bool? obscureText;
  final bool? unfocusOnSubmit;
  final bool? firstLettersUppercase;
  final IconData? suffixIcon;

  // ignore: use_key_in_widget_constructors
  const CustomTextField({
    this.hintText,
    this.obscureText,
    this.autoFocus,
    this.errorText,
    this.onSuffixIconPressed,
    this.prefix,
    this.onEditingComplete,
    this.suffixIcon,
    this.firstLettersUppercase,
    this.transparentBg,
    required this.onChanged,
    this.inputFormatters,
    this.initialValue,
    this.unfocusOnSubmit,
    this.enabled,
    this.keyboardType,
    this.controller,
  });

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool isEmpty = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 7.5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            blurRadius: 0.5,
            blurStyle: BlurStyle.normal,
            spreadRadius: 3,
            color: Colors.black.withOpacity(0.05),
            offset: Offset(-1, 1),
          )
        ],
        // border: Border.all(
        //   color: Color(0xffE8E8EA),
        // ),
      ),
      child: TextFormField(
        autofocus: widget.autoFocus ?? false,
        controller: widget.controller,
        textCapitalization: widget.firstLettersUppercase != null
            ? TextCapitalization.sentences
            : TextCapitalization.none,
        onChanged: (t) {
          widget.onChanged(t);
          setState(() {
            if (t == '') {
              isEmpty = true;
            } else {
              isEmpty = false;
            }
          });
        },
        onEditingComplete: () {
          if (widget.unfocusOnSubmit != null && widget.unfocusOnSubmit!) {
            FocusScope.of(context).unfocus();
          } else {
            FocusScope.of(context).nextFocus();
          }
          widget.onEditingComplete;
        },
        inputFormatters: widget.inputFormatters ?? [],
        keyboardType: widget.keyboardType ?? TextInputType.text,
        initialValue: widget.initialValue,
        cursorColor: const Color.fromRGBO(150, 152, 155, 1),
        obscureText: widget.obscureText ?? false,
        style: const TextStyle(
          color: Colors.black,
          fontSize: 16,
        ),
        decoration: InputDecoration(
          prefixIconConstraints:
              const BoxConstraints(maxHeight: 30, maxWidth: 30, minWidth: 30),
          prefix: widget.prefix,
          suffixIconConstraints:
              const BoxConstraints(maxHeight: 30, maxWidth: 30, minWidth: 30),
          suffixIcon: widget.suffixIcon != null
              ? GestureDetector(
                  onTap: widget.onSuffixIconPressed,
                  child: Icon(
                    widget.suffixIcon,
                    size: 20,
                    color: AppColors.lightBlue,
                  ),
                )
              : null,
          disabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          focusedBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          errorText: widget.errorText,
          fillColor: widget.enabled == null || widget.enabled!
              ? widget.transparentBg != null && widget.transparentBg!
                  ? Colors.transparent
                  : Colors.white
              : Colors.grey.shade200,
          filled: true,
          enabled: widget.enabled ?? true,
          // labelText: widget.labelText,
          hintText: widget.hintText,
          contentPadding: const EdgeInsets.symmetric(horizontal: 0),
          hintStyle: const TextStyle(
            fontSize: 16,
            color: Color.fromRGBO(150, 152, 155, 1),
          ),
        ),
      ),
    );
  }
}
