import 'package:flutter/material.dart';

class AppColors {
  static const Color strongBlue = Color(0xff30638E);
  static const Color yellowGreen = Color(0xff9FCC2E);
  static const Color lightBlue = Color(0xff9BB1FF);
  static const Color silver = Color(0xffC6C5B9);
  static const Color black = Color(0xff171717);
}
