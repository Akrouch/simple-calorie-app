String errorCodeHandler(String errorCode) {
  switch (errorCode) {
    case 'invalid-email':
      return 'Invalid email format';
    case 'user-not-found':
    case 'wrong-password':
      return 'Wrong email or password';
    default:
      return errorCode;
  }
}
