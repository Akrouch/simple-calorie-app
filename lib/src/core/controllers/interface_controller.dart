import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

import '../interface/widgets/blurred_loader.dart';
// ignore: import_of_legacy_library_into_null_safe

enum ToastType { error, warning, success }

class DialogAction {
  final String title;
  final void Function() onPressed;
  final Color? color;
  final bool? elevated;

  DialogAction({
    required this.title,
    required this.onPressed,
    this.color,
    this.elevated,
  });
}

class InterfaceController {
  /// Navigation key to be used in the MaterialApp instance
  final navigationKey = GlobalKey<NavigatorState>();

  Future<dynamic> showModal({
    required Widget widget,
    dynamic arguments,
  }) {
    return navigationKey.currentState!.push(
      PageRouteBuilder(
        settings: RouteSettings(
          arguments: arguments,
        ),
        pageBuilder: (context, animation, secondaryAnimation) => widget,
        transitionsBuilder: (
          context,
          animation,
          secondaryAnimation,
          child,
        ) {
          var begin = const Offset(0.0, 1.0);
          var end = Offset.zero;
          var curve = Curves.ease;

          var tween = Tween(begin: begin, end: end).chain(
            CurveTween(curve: curve),
          );

          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
      ),
    );
  }

  /// Shows a blurred loader on top of the app
  void showLoader() {
    BotToast.showCustomLoading(
      animationDuration: const Duration(milliseconds: 300),
      animationReverseDuration: const Duration(milliseconds: 300),
      backButtonBehavior: BackButtonBehavior.ignore,
      allowClick: false,
      ignoreContentClick: true,
      toastBuilder: (_) => const BlurredLoader(),
    );
  }

  /// Closes the blurred loader if it exists
  void closeLoader() {
    BotToast.closeAllLoading();
  }

  /// Navigates to a page
  Future<dynamic> navigateTo(
    String routeName, {
    dynamic arguments,
  }) {
    return navigationKey.currentState!.pushNamed(
      routeName,
      arguments: arguments,
    );
  }

  void popUntil(String routeName) {
    return navigationKey.currentState!.popUntil(ModalRoute.withName(routeName));
  }

  /// Navigates to a page and remove the current one from the stack
  Future<dynamic> replaceWith(
    String routeName, {
    dynamic arguments,
  }) {
    return navigationKey.currentState!.pushReplacementNamed(
      routeName,
      arguments: arguments,
    );
  }

  /// Navigates to a page and remove all pages from the stack
  Future<dynamic> replaceAllWith(
    String routeName, {
    dynamic arguments,
  }) {
    return navigationKey.currentState!.pushNamedAndRemoveUntil(
      routeName,
      (route) => false,
      arguments: arguments,
    );
  }

  /// Removes current page from the stack
  void goBack([dynamic result]) {
    navigationKey.currentState!.pop(result);
  }

  void showTopSnackBar({
    required String message,
    required ToastType type,
  }) {
    BotToast.showCustomNotification(
      align: Alignment.topCenter,
      toastBuilder: (_) => Container(
        height: 70,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
        ),
        width: double.maxFinite,
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(10),
        child: Row(
          children: [
            Container(
              width: 4,
              decoration: BoxDecoration(
                color: type == ToastType.warning
                    ? Colors.amber
                    : type == ToastType.error
                        ? Colors.red
                        : Colors.green,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8)),
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    type == ToastType.warning || type == ToastType.error
                        ? 'Attention'
                        : 'Success',
                    style: TextStyle(
                      color: type == ToastType.warning
                          ? Colors.amber
                          : type == ToastType.error
                              ? Colors.red
                              : Colors.green,
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    message,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  /// Shows a snackbar with custom color and message
  void showBottomSnackBar({
    required String message,
    required Color backgroundColor,
  }) {
    BotToast.showCustomText(
      align: Alignment.bottomCenter,
      toastBuilder: (_) => Container(
        height: 50,
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: const BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        width: double.maxFinite,
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.all(10),
        alignment: Alignment.center,
        child: Text(
          message,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
        ),
      ),
    );
  }

  /// Shows a dialog message with optional actions
  Future<dynamic> showDialogMessage({
    required String title,
    required String message,
    Color textButtonColor = Colors.blue,
    required List<DialogAction> actions,
    bool isDismissible = true,
  }) {
    var dialog = AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      title: Text(
        title,
        textAlign: TextAlign.center,
        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 15, 20, 25),
            child: Text(
              message,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 14,
                color: Color.fromRGBO(100, 102, 105, 1),
              ),
            ),
          ),
          const Divider(
            height: 0,
            color: Color(0xFFEDEFF2),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(
              actions.length,
              (index) => Expanded(
                child: GestureDetector(
                  onTap: actions[index].onPressed,
                  child: Container(
                    height: 55,
                    decoration: BoxDecoration(
                      color: actions[index].elevated != null &&
                              actions[index].elevated!
                          ? actions[index].color
                          : Colors.white,
                      border: index == actions.length - 1
                          ? null
                          : const Border(
                              right: BorderSide(
                                color: Color(0xFFEDEFF2),
                                width: 1,
                              ),
                            ),
                    ),
                    child: Center(
                      child: Text(
                        actions[index].title,
                        style: TextStyle(
                          fontSize: 16,
                          color: actions[index].elevated != null &&
                                  actions[index].elevated!
                              ? Colors.white
                              : actions[index].color,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    return showDialog(
      context: navigationKey.currentContext!,
      builder: (context) => isDismissible
          ? dialog
          : WillPopScope(
              onWillPop: () async => false,
              child: dialog,
            ),
      barrierDismissible: isDismissible,
    );
  }

  Future<dynamic> showDialogWithWidgets({
    String? title,
    String? message,
    required Widget widget,
    Color? color,
    List<DialogAction> actions = const [],
    bool isDismissible = true,
  }) {
    var dialog = AlertDialog(
      actionsPadding: const EdgeInsets.fromLTRB(0, 0, 15, 5),
      title: title != null ? Text(title) : Container(),
      content: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            if (message != null) Text(message),
            if (message != null) const SizedBox(height: 15),
            widget
          ],
        ),
      ),
      actions: List<DialogAction>.from(actions).map(
        (action) {
          if (action.title == 'Enviar' ||
              action.title == 'Confirmar' ||
              action.title == 'Criar' ||
              action.title == 'Continuar' ||
              action.title == 'Cadastrar' ||
              action.title == 'Câmera' ||
              action.title == 'Adicionar' ||
              action.title == 'Galeria' ||
              action.title == 'Remover') {
            return Container(
              constraints: const BoxConstraints(minWidth: 75, maxWidth: 100),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  primary: Colors.blue,
                ),
                onPressed: action.onPressed,
                child: Text(
                  action.title,
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            );
          }
          return TextButton(
            onPressed: action.onPressed,
            child: Text(
              action.title,
              style: TextStyle(
                color: action.color ?? Colors.blue,
              ),
            ),
          );
        },
      ).toList(),
    );

    return showDialog(
      context: navigationKey.currentContext!,
      builder: (context) => isDismissible
          ? dialog
          : WillPopScope(
              onWillPop: () async => false,
              child: dialog,
            ),
      barrierDismissible: isDismissible,
    );
  }
}
