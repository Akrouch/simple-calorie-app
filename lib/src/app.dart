import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'core/controllers/interface_controller.dart';
import 'core/providers/food_entries_provider.dart';
import 'core/providers/user_provider.dart';
import 'core/services/connectivity_service.dart';
import 'locator.dart';
import 'router.dart' as r;

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ListenableProvider.value(
          value: locator<UserProvider>(),
        ),
        ListenableProvider.value(
          value: locator<FoodEntriesProvider>(),
        ),
        ListenableProvider.value(
          value: locator<ConnectivityService>(),
        ),
      ],
      child: MaterialApp(
        scrollBehavior: const ScrollBehavior().copyWith(overscroll: false),
        navigatorKey: locator<InterfaceController>().navigationKey,
        builder: BotToastInit(),
        localizationsDelegates: GlobalMaterialLocalizations.delegates +
            [GlobalWidgetsLocalizations.delegate],
        onGenerateRoute: r.Router.generateRoute,
        navigatorObservers: [
          BotToastNavigatorObserver(),
        ],
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
