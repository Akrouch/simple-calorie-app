import 'package:get_it/get_it.dart';
import 'package:simple_calorie_app/src/core/providers/food_entries_provider.dart';
import 'package:simple_calorie_app/src/core/services/api_service.dart';
import 'package:simple_calorie_app/src/core/services/connectivity_service.dart';
import 'package:simple_calorie_app/src/core/services/hive_service.dart';

import 'core/controllers/interface_controller.dart';
import 'core/providers/user_provider.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  //CREATES ALL THE SINGLETONS
  locator.registerLazySingleton(() => InterfaceController());
  locator.registerLazySingleton(() => ApiService());
  locator.registerLazySingleton(() => HiveService());
  locator.registerLazySingleton(() => ConnectivityService());
  locator.registerLazySingleton(() => FoodEntriesProvider());
  locator.registerLazySingleton(() => UserProvider());
}
